<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="wrap">

    <h2><?php echo esc_html(get_admin_page_title()); ?></h2>
    
    <!--<form method="post" name="cleanup_options" action="../wp-content/plugins/tee20-customer-email/admin/handle-submit.php"> -->
    <form method="post" name="cleanup_options">
         <?php settings_fields($this->plugin_name); ?>
        <!-- remove some meta and generators from the <head> -->
  
        <fieldset>
            <legend class="screen-reader-text"><span>Clean WordPress head section</span></legend>
            <label for="<?php echo $this->plugin_name; ?>-customer"/>      
            <?php wp_dropdown_users(array('name' => 'customer', 'id' => $this->plugin_name . "-customer")); ?>
            
        </fieldset>

        <fieldset>
            <label for="<?php echo $this->plugin_name; ?>-images">
                Links to apparel images
            </label>
            <textarea name="image_links" id="<?php echo $this->plugin_name; ?>-images">
            </textarea>
        </fieldset>

        <?php submit_button('Save all changes', 'primary','submit', TRUE); ?>

    </form>

</div>